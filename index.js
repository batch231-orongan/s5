//console.log("Hello World!");


class Customer{
	constructor(email){
		this.email = email;
		this.cart = new Cart();
		this.orders = [		];
	}

	checkOut(){

		if(this.cart.length !== 0){

			this.orders.push(this.cart.contents);
			console.log(`Your order/s is checked out successfully!`)
		} else{
			console.log(`Your cart is empty! Add to cart now!`)
		}

		return this;
	}
}

class Product{
	constructor(name, price, isActive){
		this.name = name;
		this.price = price;
		this.isActive = true;
	}

	archive(name){
		if(this.isActive){
			this.isActive = false;
		}
		console.log(`${name} is now inactive.`);
		return this;
	}

	updatePrice(newPrice){
		this.price = newPrice;
		console.log(`Price is updated to ${newPrice}`);
		return this;
	}

}

class Cart{
	constructor(contents, totalAmount){
		this.contents = []
		this.totalAmount = 0;
	}

	addToCart(product, quantity){
		
		this.contents.push({product, quantity});
		console.log(`Your ${quantity} pcs of ${product.name} is added to your cart.`);
			
		return this;
	}

	showCartContents(){
		return this.contents;
	}

	updateProductQuantity(name, newQuantity){
		this.contents.forEach(content => {
			if(content.product.name === name){
				content.quantity = newQuantity;
				console.log(`Quantity of ${name} is updated to ${newQuantity}`);
			}});

		return this;
	}

	clearCartContents(){
		this.contents = [];
		return this;
	}

	computeTotal(){
		let total = 0;

		this.contents.forEach(content => {
			total = total + (content.product.price * content.quantity)
		})
		
		this.totalAmount = total;

		return this;
	}
}

// Instantiate Customer

let customerList = [];

let customer1 = new Customer("juan@mail.com");
customerList.push(customer1);

// customer registration
function register(email){
	if(customerList.find(customer => customer.email === email)){
		console.log(`${email} already exists!`);
	} else{
		let customerNew = new Customer(email);
		customerList.push(customerNew)

		console.log(`${email} has been registerd!`);
		return customerList;
	}

};


// Populate Product list

let productList = [];

let product1 = new Product("tshirt", 100);
let product2 = new Product("shorts", 50);
let product3 = new Product("pants", 150);

productList.push(product1);
productList.push(product2);
productList.push(product3);

// Add Product
function addProduct(name, price, isActive){
	let productNew = new Product(name, price, isActive);
	productList.push(productNew);

	console.log(`${name} has been added!`);
	return productList;
}

let cartList = [];

// test statements
// customer1.cart.addToCart(product1, 2)
// customer1.cart.addToCart(product2, 3)

// customer1.cart.computeTotal()
// customer1.cart.updateProductQuantity("shorts", 5)
// customer1.cart.showCartContents()
// customer1.cart.clearCartContents()
// customer1.checkOut()
